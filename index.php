<?php

    include 'conf.php';

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Voici les routes pour enregistrer des itinéraires Web de l'application.
| Les routes sont chargées par $_SERVER['REQUEST_URI']
|
*/

$request = $_SERVER['REQUEST_URI'];

switch ($request) {

    // Page de 1er niveau

    case '/' :
        require __DIR__ . '/src/controller/index.php';
        break;

    // Différent models AR
    case '/app?planete=terre' :
    case '/app?planete=neptune' :
    case '/app?planete=mars' :
    case '/app?planete=venus' :
        require __DIR__ . '/src/controller/app.php';
        break;

    // Autres pages

    case '/recherche' :
        require __DIR__ . '/src/controller/recherche.php';
        break;

    case '/aide' :
        require __DIR__ . '/src/controller/aide.php';
        break;

}
