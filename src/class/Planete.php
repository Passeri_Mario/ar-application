<?php

class Planete

{
    private mixed $id;
    private mixed $nom;
    private mixed $inclinaison;
    private mixed $masse;
    private mixed $gravite;
    private mixed $lune;

    /**
     * @param $id
     * @param $nom
     * @param $inclinaison
     * @param $masse
     * @param $gravite
     * @param $lune
     */
    public function __construct($id, $nom, $inclinaison, $masse, $gravite, $lune)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->inclinaison = $inclinaison;
        $this->masse = $masse;
        $this->gravite = $gravite;
        $this->lune = $lune;
    }

    public function getInfosPlanete(){
        if($this->lune === null){
            echo '<div class="content-info">
                <p>Planete : <span class="font-bold">' . $this->nom . '</span></p>
                <p>L\'inclinaison : <span class="font-bold">' . $this->inclinaison . '°</span></p>
                <p>La masse : <span class="font-bold">' . $this->masse . ' g/cm3</span></p>                        
                <p>Sa gravité : <span class="font-bold">' . $this->gravite . ' m/s2</span></p>                         
              </div>
              
              <div class="cards ' . $this->id . ' ">
                <div class="planet">
                    <div class="atmosphere">
                        <div class="surface"></div>
                    </div>
                </div>
            </div>';
        }
        else{
            echo '<div class="content-info">
                <p>Planete : <span class="font-bold">' . $this->nom . '</span></p>
                <p>L\'inclinaison : <span class="font-bold">' . $this->inclinaison . '°</span></p>
                <p>La masse : <span class="font-bold">' . $this->masse . ' g/cm3</span></p>                        
                <p>Sa gravité : <span class="font-bold">' . $this->gravite . ' m/s2</span></p>                         
                <p>Son satélite : <span class="font-bold">' . $this->lune . '</span></p>          
              </div>
              
              <div class="cards ' . $this->id . ' ">
                <div class="planet">
                    <div class="atmosphere">
                        <div class="surface"></div>
                    </div>
                </div>
            </div>';
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getInclinaison()
    {
        return $this->inclinaison;
    }

    /**
     * @param mixed $inclinaison
     */
    public function setInclinaison($inclinaison): void
    {
        $this->inclinaison = $inclinaison;
    }

    /**
     * @return mixed
     */
    public function getMasse()
    {
        return $this->masse;
    }

    /**
     * @param mixed $masse
     */
    public function setMasse($masse): void
    {
        $this->masse = $masse;
    }

    /**
     * @return mixed
     */
    public function getGravite()
    {
        return $this->gravite;
    }

    /**
     * @param mixed $gravite
     */
    public function setGravite($gravite): void
    {
        $this->gravite = $gravite;
    }

    /**
     * @return mixed
     */
    public function getLune()
    {
        return $this->lune;
    }

    /**
     * @param mixed $lune
     */
    public function setLune($lune): void
    {
        $this->lune = $lune;
    }

}