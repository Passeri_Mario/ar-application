<?php
    require 'src/class/Planete.php';

    include 'view/app.phtml';

    /**
     * @throws JsonException
     */
    function getInfosTerre(){
        $api_Terre = file_get_contents("https://api.le-systeme-solaire.net/rest/bodies/terre");
        $content_Terre = json_decode($api_Terre, true, 512, JSON_THROW_ON_ERROR);
        $Terre = new Planete(
            $content_Terre['id'],
            $content_Terre['name'],
            $content_Terre['inclination'],
            $content_Terre['mass']['massValue'],
            $content_Terre['gravity'],
            $content_Terre['moons'][0]['moon']
        );

        $Terre->getInfosPlanete();
    }

    /**
     * @throws JsonException
     */
    function getInfosNeptune(){
        $api_Neptune = file_get_contents("https://api.le-systeme-solaire.net/rest/bodies/neptune");
        $content_Neptune = json_decode($api_Neptune, true, 512, JSON_THROW_ON_ERROR);
        $Neptune = new Planete(
            $content_Neptune['id'],
            $content_Neptune['name'],
            $content_Neptune['inclination'],
            $content_Neptune['mass']['massValue'],
            $content_Neptune['gravity'],
            $content_Neptune['moons'][0]['moon']
        );

        $Neptune->getInfosPlanete();
    }


    /**
     * @throws JsonException
     */
    function getInfosMars(){
        $api_Mars = file_get_contents("https://api.le-systeme-solaire.net/rest/bodies/mars");
        $content_Mars = json_decode($api_Mars, true, 512, JSON_THROW_ON_ERROR);
        $Mars = new Planete(
            $content_Mars['id'],
            $content_Mars['name'],
            $content_Mars['inclination'],
            $content_Mars['mass']['massValue'],
            $content_Mars['gravity'],
            $content_Mars['moons'][0]['moon']
        );

        $Mars->getInfosPlanete();
    }

    /**
     * @throws JsonException
     */
    function getInfosVenus(){
        $api_Venus = file_get_contents("https://api.le-systeme-solaire.net/rest/bodies/venus");
        $content_Venus = json_decode($api_Venus, true, 512, JSON_THROW_ON_ERROR);
        $Venus = new Planete(
            $content_Venus['id'],
            $content_Venus['name'],
            $content_Venus['inclination'],
            $content_Venus['mass']['massValue'],
            $content_Venus['gravity'],
            null
        );

        $Venus->getInfosPlanete();
    }

    // fonctions pour afficher les résulats

    /**
     * @throws JsonException
     *
     */
    function getResulatsPlanetes(){
        if(htmlentities($_GET['planete']) === "terre"){
            getInfosTerre();
        }
        elseif (htmlentities($_GET['planete']) === "neptune"){
            getInfosNeptune();
        }
        elseif (htmlentities($_GET['planete']) === "mars"){
            getInfosMars();
        }
        elseif (htmlentities($_GET['planete']) === "venus"){
            getInfosVenus();
        }
    }

    function getTextures(){
        if(htmlentities($_GET['planete']) === "terre"){
            echo "public/assets/img/textures/terre.jpg";
        }
        elseif (htmlentities($_GET['planete']) === "neptune"){
            echo "public/assets/img/textures/neptune.jpg";
        }
        elseif (htmlentities($_GET['planete']) === "mars"){
            echo "public/assets/img/textures/mars.jpg";
        }
        elseif (htmlentities($_GET['planete']) === "venus"){
            echo "public/assets/img/textures/venus.jpg";
        }
    }

