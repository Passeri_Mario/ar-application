document.addEventListener('DOMContentLoaded', function() {

    let info = document.getElementById('info'); // id de la modal de la planete
    let xmarq = document.getElementById("xmark"); // bouton suppresion de la modal
    let btn = document.getElementById("information"); // bouton desktop information
    const marker = document.querySelector('a-marker'); // event ar.js marker
    let end = document.getElementById("end"); // bouton pour quitter l'application
    let planet = false; // objet non détecté

    /**
     *  Application Mobile
     */
    // Intialisation des boutons
    function getMobileInformations(){
        marker.addEventListener('markerFound', () => {
            planet = true;
            if(planet === true){
                btn.addEventListener("click", function () {
                    info.style.display="block";
                });
            }
        });
    }

    function addBtnXmarq(){
        xmarq.addEventListener('click', ()=>{
            info.style.display="none";
        })
    }

    /**
     *  Application Bureau
     */

    function supprBtnXmarq(){
        xmarq.style.display = "none";
    }

    // Intialisation des boutons
    function startButtonDesktop(){
        btn.addEventListener("click", ()=>{
            alert("Utilise : PHP 8, JavaScript, A-FRAME, TREE.JS, CSS 3, Donnée API Systeme Solaire.net");
        });
    }

    function getQrCodeDesktop(){
        info.style.display="none";
        window.addEventListener('load', () => {
            marker.addEventListener('markerFound', (e) => {
                info.style.display="block";
            });
            marker.addEventListener("markerLost",() =>{
                info.style.display="none";
            });
        })
    }

    /**
     * Function génériques
     */

    // Intialisation des boutons
    function stopApplication(){
        end.addEventListener("click",  ()=> {
            if ( confirm( "Voulez-vous réellement quitter le programme ?" ) ) {
                window.location.href = "/";
            } else {
               exit();
            }
        });
    }

    /**
     * Traitement du programme en fonction du type de device
     */
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        getMobileInformations();
        addBtnXmarq();
        stopApplication();
    }
    else {
        startButtonDesktop()
        getQrCodeDesktop();
        stopApplication();
        supprBtnXmarq();
    }
});